package ru.t1.rleonov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public final class TaskStartByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    @Nullable
    private final Status status = Status.IN_PROGRESS;

    public TaskStartByIdRequest(@Nullable String token) {
        super(token);
    }

}
