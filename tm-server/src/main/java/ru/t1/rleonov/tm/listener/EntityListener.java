package ru.t1.rleonov.tm.listener;

import org.hibernate.event.spi.*;
import org.hibernate.persister.entity.EntityPersister;
import org.jetbrains.annotations.NotNull;

public final class EntityListener implements PostInsertEventListener, PostDeleteEventListener, PostUpdateEventListener {

    @NotNull
    private final JmsLoggerProducer jmsLoggerProducer;

    public EntityListener(@NotNull final JmsLoggerProducer jmsLoggerProducer) {
        this.jmsLoggerProducer = jmsLoggerProducer;
    }

    @Override
    public void onPostDelete(PostDeleteEvent postDeleteEvent) {
        log(OperationType.DELETE, postDeleteEvent.getEntity());
    }

    @Override
    public void onPostInsert(PostInsertEvent postInsertEvent) {
        log(OperationType.INSERT, postInsertEvent.getEntity());
    }

    @Override
    public void onPostUpdate(PostUpdateEvent postUpdateEvent) {
        log(OperationType.UPDATE, postUpdateEvent.getEntity());
    }

    @Override
    public boolean requiresPostCommitHanding(EntityPersister entityPersister) {
        return false;
    }

    public void log(final OperationType operationType, final Object entity) {
        jmsLoggerProducer.send(new OperationEvent(operationType, entity));
    }

}
