package ru.t1.rleonov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {

    @Nullable
    Session findOneById(@NotNull String id);

}
